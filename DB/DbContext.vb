﻿Imports Microsoft.SqlServer
Imports MySql.Data.MySqlClient
Imports System.Configuration
Public Class DbContext

    Public Function Conectar() As MySqlConnection

        Dim con = New MySqlConnection("server=127.0.0.1;uid=root;pwd=;database=Gastos")
        Return con

    End Function

    Public Function ObtenerUsuario(usuario As String, password As String) As Integer

        Dim con = Conectar()
        Try
            con.Open()
            Dim Comand = New MySqlCommand("SELECT BuscarUsuario(@nombre, @password)", con)
            Comand.Parameters.AddWithValue("@nombre", usuario)
            Comand.Parameters.AddWithValue("@password", password)
            Dim resultado = Comand.ExecuteScalar()
            con.Close()
            Return resultado
        Catch ex As Exception
            MsgBox("Base de datos no conetada", 1, "Error")

        End Try


    End Function



End Class
